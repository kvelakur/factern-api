package com.factern.data.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;

public class NodeClassUtilTest {
    @Test
    public void testFromValueCaseInsensitive() {
        testEntity("entity");
        testEntity("ENTITY");
        testEntity("Entity");
        testEntity("eNTITY");
        testEntity("eNtItY");
    }

    @Test
    public void testNotExists() {
        assertEquals(Optional.empty(), NodeClassUtil.fromValueCaseInsensitive("foobar"));
        assertEquals(Optional.empty(), NodeClassUtil.fromValueCaseInsensitive(""));
        assertEquals(Optional.empty(), NodeClassUtil.fromValueCaseInsensitive(null));
    }

    private void testEntity(final String value) {
        Optional<NodeClass> nodeClass = NodeClassUtil.fromValueCaseInsensitive(value);
        assertTrue(nodeClass.isPresent());
        assertEquals(NodeClass.ENTITY, nodeClass.get());
    }
}
