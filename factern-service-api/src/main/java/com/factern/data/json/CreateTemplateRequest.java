package com.factern.data.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.vertx.core.json.JsonArray;
import lombok.Data;

@Data
public class CreateTemplateRequest {
    private String name;
    private String description;
    private String parentId;

    @JsonDeserialize(using = JsonArrayDeserializer.class)
    private final JsonArray memberIds;
}
