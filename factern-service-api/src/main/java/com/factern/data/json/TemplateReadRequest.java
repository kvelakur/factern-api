package com.factern.data.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.vertx.core.json.JsonArray;
import lombok.Data;

/**
 * For reading a template.
 */
@Data
public class TemplateReadRequest {
    private final String nodeId;
    private final String templateId;
    private final String defaultStorageId;

    @JsonDeserialize(using = JsonArrayDeserializer.class)
    private final JsonArray template;
}
