package com.factern.data.json;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import com.google.common.collect.ImmutableMap;

/**
 * Utility for dealing with {@link NodeClass}.
 */
public final class NodeClassUtil {
    private static final Map<String, NodeClass> nodeClassMap = createNodeClassMap();

    private static Map<String, NodeClass> createNodeClassMap() {
        ImmutableMap.Builder<String, NodeClass> builder = ImmutableMap.builder();

        Arrays.stream(NodeClass.values())
            .forEach(nodeClass -> builder.put(nodeClass.toString().toLowerCase(), nodeClass));

        return builder.build();
    }

    private NodeClassUtil() {
    }

    public static Optional<NodeClass> fromValueCaseInsensitive(final String value) {
        return Optional.ofNullable(value)
            .map(String::toLowerCase)
            .map(nodeClassMap::get);
    }
}
