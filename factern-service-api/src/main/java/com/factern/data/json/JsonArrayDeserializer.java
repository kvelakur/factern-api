package com.factern.data.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import io.vertx.core.json.JsonArray;
import lombok.extern.slf4j.Slf4j;

/**
 * Handles deserializing a JSON subtree as a JsonArray.
 */
@Slf4j
public class JsonArrayDeserializer extends StdDeserializer<JsonArray> {

    public JsonArrayDeserializer() {
        super(JsonArray.class);
    }

    @Override
    public JsonArray deserialize(final JsonParser p, final DeserializationContext ctxt)
        throws IOException {

        return new JsonArray(p.readValueAsTree().toString());
    }
}
