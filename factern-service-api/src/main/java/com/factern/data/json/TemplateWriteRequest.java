package com.factern.data.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.vertx.core.json.JsonArray;
import lombok.Data;

/**
 * For writing a template.
 *
 * Anonymous template is a JSON array of field type id strings or JSON objects whose key is the field type id and whose
 * value is a JSON array.
 */
@Data
public class TemplateWriteRequest {
    private final String nodeId;
    private final String templateId;
    private final String sourceNodeId;
    private final String defaultStorageId;

    @JsonDeserialize(using = JsonArrayDeserializer.class)
    private final JsonArray values;

    @JsonDeserialize(using = JsonArrayDeserializer.class)
    private final JsonArray template;

    @JsonDeserialize(using = JsonArrayDeserializer.class)
    private final JsonArray document;
}
