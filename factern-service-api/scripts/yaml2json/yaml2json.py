#!/usr/bin/env python

import argparse
import json
import os
import yaml

desc = "Take a root of a yaml source tree and generate json files"

parser = argparse.ArgumentParser(description=desc)

parser.add_argument("src",
                    metavar="src",
                    nargs='+',
                    help="the path to the yaml source files")

parser.add_argument("-o",
                    "--output-dir",
                    help="the destination directory of generated files")

args = parser.parse_args()

outdir = args.output_dir or "target"

src_root = args.src[0]

cwd = os.getcwd()
outdir = os.path.join(cwd, outdir)

os.chdir(src_root)
for root, subdirs, files in os.walk('.'):
    for file in files:

        if not file.endswith('.yaml'):
            continue
        src_file_path = os.path.join(root, file)
        with open(src_file_path, 'r') as f:
            yaml_dict = yaml.load(f)
            json_str = json.dumps(yaml_dict, sort_keys=True, indent=4, separators=(',', ': '))

        target_dir = os.path.join(outdir, root)
        print('Target dir %s' %target_dir)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        target_file = os.path.join(target_dir, file.replace('.yaml', '.json'))
        with open(target_file, 'w') as f:
            print('Writing to %s' %target_file)
            f.write(json_str)

os.chdir(cwd)
