#!/usr/bin/env python

import argparse
from contextlib import closing
import copy
from jinja2 import Template
import json
import logging
import os
import urllib2
import yaml

class Swagger2Api(object):
    """
    For converting a swagger API to a factern API - i.e. Java event bus
    handlers and jsonschema for the in/out payloads.

    Note that swagger supports and encourages the use of jsonschema allOf.
    However, jsonschema2pojo does NOT support allOf. As such, we explicitly
    evaluate allOf, and include the composed definitions directly into then
    including definition.
    """
    def __init__(self, swagger_file, outdir, package):
        self.swagger_file = swagger_file
        self.outdir = outdir
        self.data = self.get_swagger()
        self.api_version = self.data['basePath'].strip('/')
        self.package = package + '.' + self.api_version
        self.schema_definitions = self.data['definitions']
        self.schema_paths = self.data['paths']

        if not os.path.exists(self.outdir):
            os.makedirs(self.outdir)

        self.json_schema_outdir = os.path.join(self.outdir, 'json', self.api_version)
        if not os.path.exists(self.json_schema_outdir):
            os.makedirs(self.json_schema_outdir)

        self.java_handler_outdir = os.path.join(self.outdir, 'java')
        if not os.path.exists(self.java_handler_outdir):
            os.makedirs(self.java_handler_outdir)

        self.java_handler_binder_outdir = os.path.join(self.outdir, 'java')
        if not os.path.exists(self.java_handler_binder_outdir):
            os.makedirs(self.java_handler_binder_outdir)

    def upperfirst(self, x):
        """Convert first letter in string to uppercase"""
        return x[0].upper() + x[1:]

    def package_to_path(self, path_root, package):
        """Converts a java package name to a file path, creating as necessary"""
        path = os.path.join(path_root, *package.split('.'))
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def get_template(self, template_name):
        """Loads a template from the filesystem"""
        with open(os.path.join(os.path.dirname(__file__), 'templates', template_name), 'r') as f:
            return Template(f.read())

    def get_build_dir(self):
        """The root of the build system"""
        return self.outdir

    def get_swagger(self):
        """Loads the swagger rules from the filesystem"""
        with closing(urllib2.urlopen(self.swagger_file)) as f:
            return yaml.load(f)

    def cleanup_refs(self, json_schema):
        """
        Somewhat complicated logic for converting an internal jsonschema ref
        to an external ref.  This is necessary because each jsonschema object
        defined in swagger is placed in its own file, so the internal refs
        must become external refs
        """
        if not isinstance(json_schema, dict):
            return
        for key in json_schema:
            val = json_schema[key]
            if key == '$ref' and '#' in val:
                _, name = val.rsplit('/', 1)
                json_schema[key] = name + '.json'
            elif isinstance(val, list):
                for item in val:
                    self.cleanup_refs(item)
            else:
                self.cleanup_refs(val)

    def merge_into(self, item, target):
        """
        Merges two dictionaries. Note that any jsonschema $ref maps are
        explicitly extracted and merged. Meant as a utility method for
        transform_allof.
        """
        if not isinstance(item, dict) or not isinstance(target, dict):
            return
        for key in item.keys():
            if key == '$ref':
                _, name = item[key].rsplit('/', 1)
                val = self.schema_definitions[name]
                self.merge_into(val, target)
            else:
                val = item[key]
                if key not in target:
                    target[key] = copy.deepcopy(val)
                else:
                    self.merge_into(val, target[key])


    def transform_allof(self, json_schema):
        """
        allOf is used by swagger for schema composition. jsonschema2pojo does
        not support this, so must programmatically convert allOf to explicit
        composition.
        """
        if not isinstance(json_schema, dict):
            return
        for key in json_schema.keys():
            val = json_schema[key]
            if key == 'allOf':
                if isinstance(val, list):
                    first = None
                    for item in val:
                        if first is None:
                            first = item
                        else:
                            self.merge_into(item, json_schema)
                    if first is not None:
                        json_schema['extends'] = first
                del json_schema[key]
            elif isinstance(val, list):
                for item in val:
                    self.transform_allof(item)
            else:
                self.transform_allof(val)

    def output_generated(self, src, output_file_name):
        """Outputs a generated file"""
        with open(output_file_name, 'w') as f:
            f.write(src)

    def output_json(self, src, output_file_name):
        """Outputs a generated json file"""
        out_file_path = os.path.join(self.json_schema_outdir, output_file_name)
        self.output_generated(json.dumps(src, indent=4, separators=(',', ': ')), out_file_path)

    def output_templated(self, template, output_file_name, **template_args):
        """Outputs a file generated via a template"""
        with open(output_file_name, 'w') as f:
            f.write(template.render(**template_args) + '\n')

    def set_schema(self, json_map):
        """
        Adds $schema to each json object to be written to a file. This is to
        ensure it is consumable via jsonschema2pojo
        """
        json_map['$schema'] = 'http://json-schema.org/draft-04/schema#'

    def output_base_classes(self):
        """
        Output the base Request and Response schemas.
        """
        response_schema = {
            'type': 'object',
            'javaType': self.package + '.json.' + 'Response',
            'properties': {
            }
        }
        self.set_schema(response_schema)
        self.output_json(response_schema, 'Response.json')

        template = self.get_template('RequestTemplate.json')
        template_context = {
            'package': self.package + '.json',
            'className': 'Request'
        }
        output_file_name = os.path.join(self.json_schema_outdir, 'Request.json')
        self.output_templated(template,
                              output_file_name,
                              request=template_context)

    def output_definitions(self, definitions):
        """Outputs the swagger definitions, one file per definition"""
        self.cleanup_refs(definitions)
        for definition in definitions:
            defn = definitions[definition]
            self.set_schema(defn)
            if definition.endswith('Response') and not 'extends' in defn:
                # All responses definitions extend the base Response scheme
                defn['extends'] = {
                    '$ref': 'Response.json'
                }
            defn['javaType'] = self.package + '.json.' + self.upperfirst(definition)
            self.output_json(defn, self.upperfirst(definition + '.json'))

    def output_request_method(self, op, params):
        """
        Schemas for the requests are taken from the swagger parameters section
        of the swagger #/paths/{path}/{method} section. Only parameters of type
        'path', 'query' or 'body' are used
        """
        # Note that all requests extend the base Request schema
        schema = {
            'type': 'object',
            'extends': {
                '$ref': 'Request.json'
            },
            'javaType': self.package + '.json.' + self.upperfirst(op) + 'Request',
            'properties': {
            }
        }
        self.set_schema(schema)

        for param in params:
            if not param['in'] in ['path', 'body', 'query']:
                # headers etc. are handled web service side
                continue
            desc = schema['properties'][param['name']] = {}
            if 'schema' in param:
                for schema_param in param['schema']:
                    desc[schema_param] = param['schema'][schema_param]
            else:
                for param_prop in param:
                    if param_prop in ['in', 'name']:
                        # These are irrelevant with respect to jsonschema
                        continue
                    desc[param_prop] = param[param_prop]

        self.cleanup_refs(schema)

        self.output_json(schema, self.upperfirst(op + 'Request.json'))

        return self.upperfirst(op + 'Request')

    def output_response_methods(self, op, responses):
        if len(responses) > 1:
            # This could certainly be extended to support something more
            # general, but at this time, this is unnecessary
            raise Exception('Only single responses are supported at this time')

        for response_code in responses:
            response = responses[response_code]
            for part in response:
                if part == 'schema':
                    for ref in response[part]:
                        if ref == '$ref':
                            _, class_name = response[part][ref].rsplit('/', 1)
                            return (response_code, class_name)
            # No response body
            return (response_code, None)
        # Even empty responses need to be specified
        raise Exception('No responses specified')

    def output_method(self, method):
        """Outputs jsonschema, one file for each method response and request"""
        op = method['operationId']
        params = method['parameters']
        responses = method['responses']

        request_class_name = self.output_request_method(op, params)

        response_code, response_class_name = self.output_response_methods(op, responses)

        template = self.get_template('ActionHandlerTemplate.java')

        class_package = self.package + '.verticle.action'

        java_handler = {
            'package': class_package,
            'apiPackage': self.package + '.json',
            'name': self.upperfirst(op),
            'requestClass': request_class_name,
            'responseClass': response_class_name or "Void",
            'expectedStatusCode': response_code,
            'address': op
        }
        output_file_name = os.path.join(self.package_to_path(self.java_handler_binder_outdir, class_package),
                                        self.upperfirst(op + 'ActionHandler.java'))

        self.output_templated(template,
                              output_file_name,
                              handler=java_handler)

    def output_handlers(self, paths):
        """Outputs a java handler for each swagger method"""
        methods = [paths[path][method] for path in paths for method in paths[path]]
        for method in methods:
            self.output_method(method)

        self.output_handlers_binder(methods)

    def output_handlers_binder(self, methods):
        """Output the java guice binder used to register each java handler"""
        ops = [method['operationId'] for method in methods]

        class_package = self.package + '.verticle.di'
        java_handler = {
            'diPackage': class_package,
            'handlers': [{
                'package': self.package + '.verticle.action',
                'name': self.upperfirst(op + 'ActionHandler')
            } for op in ops]
        }
        output_file_name = os.path.join(self.package_to_path(self.java_handler_binder_outdir, class_package),
                                        self.upperfirst('ActionHandlerBinder.java'))
        self.output_templated(
            self.get_template('ActionHandlerBinderTemplate.java'),
            output_file_name,
            binding=java_handler)

    def swagger2api(self):
        """Generates all relevant json and java files from the swagger defn"""
        self.transform_allof(self.schema_definitions)

        self.output_base_classes()

        self.output_definitions(self.schema_definitions)

        self.output_handlers(self.schema_paths)

desc = "Take a swagger.yaml and output event bus handlers and request/response schemas"

parser = argparse.ArgumentParser(description=desc)

parser.add_argument("src",
                    metavar="src",
                    nargs='+',
                    help="the path to the swagger yaml source file")

parser.add_argument("-o",
                    "--output-dir",
                    help="the destination directory of generated files")

parser.add_argument("-p",
                    "--package",
                    help="the package root of the generated files")

args = parser.parse_args()

outdir = args.output_dir or "target"

package = args.package or ""

swagger_file = args.src[0]

Swagger2Api(swagger_file, outdir=outdir, package=package).swagger2api()
