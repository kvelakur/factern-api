{#
/**
 * jinja2 template for the java action handler for a swagger route
 */
#}
package {{ handler.package }};

import java.util.function.Function;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import {{handler.apiPackage}}.{{handler.requestClass}};
{% if handler.responseClass != "Void" %}
import {{handler.apiPackage}}.{{handler.responseClass}};
{% endif %}
import com.finovertech.actionHandler.ActionHandler;
import com.finovertech.di.ActionHandlerBindUtils;
import com.finovertech.fri.action.dto.Action;
import com.finovertech.fri.common.ServicesEnum;
import com.finovertech.vertx.response.Response;
import io.reactivex.Observable;

/**
 * Generated from swagger.yaml. Do not edit directly
 */
@Singleton
@SuppressWarnings({"checkstyle:LineLength", "checkstyle:AbbreviationAsWordInName", "checkstyle:TypeName"})
public class {{ handler.name }}ActionHandler implements ActionHandler<{{ handler.requestClass }}, {{ handler.responseClass }}> {

    /**
     * Public to allow you to do your binding, and/or use providers
     */
    public static final TypeLiteral<Function<{{ handler.requestClass }}, Observable<Response<{{ handler.responseClass }}>>>> TYPE = new TypeLiteral<Function<{{ handler.requestClass }}, Observable<Response<{{ handler.responseClass }}>>>>() {
    };
    /**
     * Public to allow you to do your binding, and/or use providers
     */
    public static final Named NAME = Names.named("{{ handler.name }}");
        
    @Inject
    @Named("{{ handler.name }}")
    private Function<{{ handler.requestClass }}, Observable<Response<{{ handler.responseClass }}>>> handlerFunction;
    
    @Override
    public final Observable<Response<{{ handler.responseClass }}>> handleRequest(final {{ handler.requestClass }} request) {
        return handlerFunction.apply(request);
    }

    @Override
    public final Class<{{ handler.requestClass }}> getRequestType() {
        return {{ handler.requestClass }}.class;
    }

    @Override
    public final Class<{{ handler.responseClass }}> getResponseType() {
        return {{ handler.responseClass }}.class;
    }
    
    public static Module module() {
        return ActionHandlerBindUtils.actionHandlerBindingModule(new Action(ServicesEnum.data, "{{ handler.address }}").toString(), {{ handler.name }}ActionHandler.class);
    }
    
    public static Module implementation(Class<? extends Function<{{ handler.requestClass }}, Observable<Response<{{ handler.responseClass }}>>>> implClass) {
        return new AbstractModule() {
            @Override
            protected void configure() {
                bind(TYPE)
                    .annotatedWith(NAME)
                    .to(implClass);
            }
        };
    }
    
    public static Module implementation(Function<{{ handler.requestClass }}, Observable<Response<{{ handler.responseClass }}>>> impl) {
        return new AbstractModule() {
            @Override
            protected void configure() {
                bind(TYPE)
                    .annotatedWith(NAME)
                    .toInstance(impl);
            }
        };
    }

    public static Module defaultImplementation() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                bind(TYPE)
                    .annotatedWith(NAME)
                    .toInstance(req -> Observable.just(
                        {% if handler.responseClass == "Void" %}
                            Response.<{{ handler.responseClass }}>builder().setStatus({{ handler.expectedStatusCode }}).build()
                        {% else %}
                            Response.<{{ handler.responseClass }}>builder().setStatus({{ handler.expectedStatusCode }}).setData(new {{ handler.responseClass }}()).build()
                        {% endif %}
                        ));
            }
        };
    }
}
