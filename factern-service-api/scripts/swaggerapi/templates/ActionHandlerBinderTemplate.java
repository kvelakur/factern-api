{#
/**
 * jinja2 template for the java class responsible for action handler guice bindings
 */
#}
package {{ binding.diPackage }};

import com.google.inject.AbstractModule;
{% for handler in binding.handlers %}
import {{handler.package}}.{{handler.name}};
{% endfor %}

/**
 * Bindings generated from swagger.yaml. Do not edit directly
 */

@SuppressWarnings({"checkstyle:LineLength", "checkstyle:AbbreviationAsWordInName", "checkstyle:TypeName",
    "checkstyle:JavaNCSS", "checkstyle:ExecutableStatementCount", "PMD.ExcessiveImports", "PMD.ExcessiveMethodLength"})
public class ActionHandlerBinder extends AbstractModule {
    @Override
    protected void configure() {
        {% for handler in binding.handlers %}
        install({{ handler.name }}.module());
        install({{ handler.name }}.defaultImplementation());
        {% endfor %}
    }
}
