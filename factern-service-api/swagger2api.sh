#!/bin/bash

# Exit if a command fails
set -e

pip install virtualenv

if [ ! -d venv ]; then
    virtualenv venv
fi
if [ -d venv/Scripts ]; then
    source venv/Scripts/activate
else
    source venv/bin/activate
fi
pip install -r requirements.txt
python scripts/swaggerapi/swagger2api.py $3 -o $1 -p $2
deactivate
